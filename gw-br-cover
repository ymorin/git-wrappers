#!/bin/bash
# (C) 2014, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3,
# see LICENSE.GPLv2 and LICENSE.GPLv3 in this repository

gw_cover() {
    local opts mode
    local o O
    local cover_file branch tag

    o="hdev"
    O="help,delete,edit,view"
    opts="$( getopt -n "${my_name}" -o "${o}" -l "${O}" -- "${@}" )"
    eval set -- "${opts}"

    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help)     gw_show_help cover; exit 0;;
        (-v|--view)     mode="view"; shift;;
        (-e|--edit)     mode="edit"; shift;;
        (-d|--delete)   mode="delete"; shift;;
        (--)            shift; break;;
        esac
    done
    : "${mode:=view}"
    if [ -n "${1}" ]; then
        branch="${1}"
    else
        branch="$( git rev-parse --abbrev-ref HEAD )"
    fi
    if ! gw_is_topical_branch "${branch}"; then
        error 'not editing cover-letter for non-topical branch %s\n' "${branch}"
    fi
    tag="base/${branch}"

    case "${mode}" in
    (view)
        gw_branch_get_cover "${branch}" |gw_should_page
        ;;
    (edit)
        # Re-use git's internal COMMIT_EDITMSG so editors know
        # how to do syntax highlighting.
        cover_file="${git_dir}/COMMIT_EDITMSG"
        {
            if gw_branch_has_cover "${branch}" "cover"; then
                gw_branch_get_cover "${branch}" "cover"
            fi
            cat <<-_EOF_
				
				# Please enter the cover-letter as a standard git commit message.
				# Lines starting with # will be ignored.
				#
				# Branch  : ${branch}
_EOF_
            if gw_branch_has_base "${branch}"; then
                printf '# Based on: %s\n' "$( gw_branch_get_base "${branch}" )"
            else
                printf '# Based on: (null)\n'
            fi
            cat <<-_EOF_
				#
				# Here's the one-line log of the branch, most recent commit first:
_EOF_
            gw_log --oneline |sed -r -e 's/^/#   /'
        } >"${cover_file}"
        ${EDITOR:-vi} "${cover_file}"
        gw_branch_set_cover "${branch}" <"${cover_file}"
        rm -f "${cover_file}"
        ;;
    (delete)
        gw_branch_rm_cover "${branch}"
        ;;
    esac
}

gw_cover_help() {
    cat <<_EOF_
git-br cover:
    Manage the cover-letter for a branch.

    See 'git br -h' for more information about git-br.
_EOF_
}

#-----------------------
# vim: ft=sh tabstop=4
