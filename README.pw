GIT-PW(1)                   Git-wrappers Manual                 GIT-PW(1)

NAME
    git-pw - Manage patches from patchwork

SYNOPSIS
    git pw [pwc] [-r|--refresh] [-m|--master-ok] [-p|--project PRJ]
                 [-s|--state STATE] [-a|--author AUTHOR]
                 [-R|--set-max-age AGE] [-M|--set-master-ok true|false]
                 [-P|--set-project PRJ] [FILTER]

DESCRIPTION
    git-pw provides a set of commands to manage patches from patchwork.

    For now, it can only list patches and apply them. In the future, it
    will also be able to update their status on patchwork.

SUBCOMMANDS
    pwc
        List all patches from patchwork that are in state STATE (New by
        default); the list is cached locally for AGE seconds (600s by
        default). -r will force a refresh of the list even if it is not
        yet AGE seconds old, and -R will change the default for this
        repository.

        By default, pwc refuses to apply patches to the master branch,
        unless overriden with -m; -M changes the default for this
        repository.

        -P sets the Patchwork project to get patches from for this
        repository; -p overrides that once. If not project is set, the
        default project from pwclient is used, if any. If no project
        is specified, this is an error.

        Patches can be filtered by author with -a.

        Patches can be filtered by subject by specifying FILTER, as an
        extended regular expresion (as understood by grep -E).

User Commands               Git-wrappers Manual                 GIT-PW(1)
